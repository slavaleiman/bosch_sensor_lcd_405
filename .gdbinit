target extended localhost:4242
set print pretty on
set pagination off

define pevents
  set $events = $arg0
  while $events != 0
    p/x *($events)
    set $events = $events->next
  end
end

define ptrack
  p/x *seq.scene->track
end

define pscenes
  set $scene = seq.scenes->first
  while $scene != 0
    p *(scene_t*)$scene->data
    set $scene = $scene->next
  end
end

define pscene_tracks
  set $track = seq.scene->tracks->first
  while $track != 0
    p *(midi_track_t*)$track->data
    set $track = $track->next
  end
end

define notes
  set $track_num = $arg0
    set $note = seq.scene->track[track_num]->notes
  set $count = 0
  if seq.input_count != seq.scene->track[track_num]->notes_count
    printf "WARNING input %d != saved %d\n", seq.input_count, seq.scene->track[track_num]->notes_count
  end
  while $note != 0
      printf "0x%08x %02d: \tT:0x%4x \tTdelta:0x%04x n:%02d len:%02d\n", $note->message, $count, $note->timemark & 0xFFFFF, $note->timemark >> 20, ($note->message & 0xFF0000)>>16, $note->length
    set $count = $count + 1
    set $note = $note->next
  end
end

define input_notes
  set $event = seq.input_events
  set $count = 0
  while $event != 0
    if ($event->message & 0x9000) == 0x9000
      printf "0x%0004x %02d: t:%4d n:%02d on\n", $event->message, $count, $event->timemark, ($event->message & 0xFF0000)>>16
    else
      printf "0x%0004x %02d: t:%4d n:%02d off\n", $event->message, $count, $event->timemark, ($event->message & 0xFF0000)>>16
    end
    set $count = $count + 1
    set $event = $event->next
  end
end

define output_notes
  set $event = seq.scene->output_events
  set $count = 0
  while $event != 0
    if ($event->message & 0x9000) == 0x9000
      printf "0x%0004x %02d: t:%4d n:%02d on\n", $event->message, $count, $event->timemark, ($event->message & 0xFF0000)>>16
    else
      printf "0x%0004x %02d: t:%4d n:%02d off\n", $event->message, $count, $event->timemark, ($event->message & 0xFF0000)>>16
    end
    set $count = $count + 1
    set $event = $event->next
  end
end

#b TIM1_UP_IRQHandler
b fill_input_buffer
