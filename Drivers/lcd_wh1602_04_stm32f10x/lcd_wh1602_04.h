#ifndef LCD_WH1602_04_H
#define LCD_WH1602_04_H

#include "stm32f4xx.h" 
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"

4 - PB4
6 - PB3
11 - PC12
12 - PD2
// TODO
#define lcd44780_port				GPIOB
#define lcd44780_pin_RS				// GPIO_PIN_10
#define lcd44780_pin_E				// GPIO_PIN_11
#define lcd44780_pins_data_0		GPIO_PIN_14
#define lcd44780_pins_data_1		GPIO_PIN_15
#define lcd44780_pins_data_2		GPIO_PIN_12
#define lcd44780_pins_data_3		GPIO_PIN_13

#define lcd44780_RCC_ENABLE()		__HAL_RCC_GPIOB_CLK_ENABLE()// RCC_APB2Periph_GPIOB 
#define lcd44780_offset 			12

#define lcd44780_RS_1 HAL_GPIO_WritePin(lcd44780_port, lcd44780_pin_RS, GPIO_PIN_SET)
#define lcd44780_E_1  HAL_GPIO_WritePin(lcd44780_port, lcd44780_pin_E, GPIO_PIN_SET)
#define lcd44780_RS_0 HAL_GPIO_WritePin(lcd44780_port, lcd44780_pin_RS, GPIO_PIN_RESET)
#define lcd44780_E_0  HAL_GPIO_WritePin(lcd44780_port, lcd44780_pin_E, GPIO_PIN_RESET)

void lcd44780_delay(unsigned int p);
void lcd44780_ClearLCD(void);
void lcd44780_SetLCDPosition(char x, char y);
void lcd44780_ShowChar(unsigned char c);
void lcd44780_ShowStr(unsigned char *s);
void lcd44780_init_pins(void);
void lcd44780_init(void);

#endif
